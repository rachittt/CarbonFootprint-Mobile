import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { newColors } from '../config/helper';

class FriendsTabBar extends React.Component {
    render() {
        return (
            <View style={[styles.tabs, this.props.style || {}]} testID="friendstabbar-component">
                {this.props.tabs.map((tab, index) => {
                    let overrideStyles = index == this.props.activeTab ? styles.activeStyle : {};
                    return (
                        <TouchableOpacity
                            key={tab}
                            onPress={() => this.props.goToPage(index)}
                            style={[styles.tab, overrideStyles]}
                        >
                            <Text style={styles.tabText}>{tab}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    }
}

FriendsTabBar.propTypes = {
    tabs: PropTypes.array.isRequired
};

const styles = StyleSheet.create({
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        borderRadius: 3
    },
    tabText: {
        color: newColors.black,
        fontFamily: 'Poppins'
    },
    tabs: {
        flexDirection: 'row',
        paddingVertical: 20,
        paddingHorizontal: 20,
        backgroundColor: newColors.secondary
    },
    activeStyle: {
        backgroundColor: 'rgba(0,0,0,0.12)'
    }
});

export default FriendsTabBar;
