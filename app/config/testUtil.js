import { shallow } from 'enzyme';
import * as firebase from 'firebase';
import * as admin from 'firebase-admin';
import { firebaseConfigForTesting } from './keys';
import serviceAccount from './serviceAccountKey.js';
import React from 'react';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import checkPropTypes from 'check-prop-types';
import rootReducer from '../reducers/rootReducer';

/**
 * Factory function for returning the redux store with middlewares and rootreducer.
 * @param {object} initialState
 * @returns redux store for components connected with redux.
 */
export const storeFactory = () => {
    let middlewares = [thunk];
    return createStore(rootReducer, applyMiddleware(...middlewares));
};

/**
 * Factory function to create a ShallowWrapper for the Component passed in
 * @function setup
 * @param {React.Component} Component - ShallowWrapper of this Component is returned
 * @param {object} props - Component props passed to the Component
 * @param {object} state - Component state.
 * @param {bool} isConnected - true if component coonnected to store
 * @returns {ShallowWrapper}
 */
export const setup = (Component, props = {}, initialState = null, isConnected = false) => {
    let store = null;

    if (isConnected) {
        store = storeFactory();
    }
    let wrapper = isConnected
        ? shallow(<Component store={store} {...props} />).dive()
        : shallow(<Component store={store} {...props} />);
    if (initialState && !isConnected) wrapper.setState(state);

    return wrapper;
};

// export const setup = (Component,props={},initialState=null)=>{
//     let wrapper = shallow(<Component {...props} />)
//     if(initialState ) wrapper.setState(state)

//     return wrapper
// }

/**
 * Factory function to return the ShallowWrapper with the root as node having testID = val.
 * @param {ShallowWrapper} wrapper - Given ShallowWrapper to search within.
 * @param {string} val - Value of testID attribute for search.
 * @return {ShallowWrapper} - Return ShallowWrapper containing node(s) with the given testID attribute
 */
export const findByAttr = (wrapper, val) => {
    return wrapper.find(`[testID="${val}"]`);
};

/**
 * Checks if the required props are being passed to a component.
 * @param {React.Component} component
 * @param {object} conformingProps
 */
export const checkProps = (component, conformingProps) => {
    const propError = checkPropTypes(component.propTypes, conformingProps, 'prop', component.name);
    return propError;
};

/**
 * Overrides initial state properties by those passed into action payload.
 * @param {object} EXPECTED_INITIAL_STATE
 * @param {object} obj - action object
 * @returns {object} mergedObject
 */

export const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

// Initialising clientSDK
export function initFirebase() {
    // let FirebaseServer = require('firebase-server');
    // Spin a new web server on localhost to save up-down-time.
    // new FirebaseServer(5000, 'localhost', {});
    // if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfigForTesting);
    // }
}

// Initialising adminSDK
export function initAdmin() {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: firebaseConfigForTesting.databaseURL
    });
}

// Wipes out the entire database and also the authenticated users data
export let wipeDatabase = async () => {
    // Displays and removes all users from database
    //List batch of users, 20 at a time.
    await admin
        .auth()
        .listUsers(100)
        .then(function(listUsersResult) {
            console.log('All Users registered list: ');
            listUsersResult.users.forEach(userRecord => {
                console.log(`User record(${userRecord.uid}): ${userRecord.email}`);
            });

            console.log('Wiping the users list above.');
            listUsersResult.users.forEach(async userRecord => {
                await admin
                    .auth()
                    .deleteUser(userRecord.uid)
                    .then(function() {
                        console.log('Successfully deleted user with email id:' + userRecord.email);
                    })
                    .catch(function(error) {
                        console.log('Error deleting user:', error);
                    });
            });
        })
        .catch(function(error) {
            console.log('Error listing users:', error);
        });

    // Removes all enteries in database
    // await admin.database().ref().remove();
};
