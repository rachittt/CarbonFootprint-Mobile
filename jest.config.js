module.exports = {
    preset: 'react-native',
    verbose: true,
    transformIgnorePatterns: ['node_modules/(?!@ngrx|(?!deck.gl)|ng-dynamic)'],
    transform: {
        '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js'
    },
    clearMocks: true,
    setupFilesAfterEnv: ['<rootDir>/__mocks__/setupFile.js']
};
