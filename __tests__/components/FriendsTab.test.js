import FriendsTab from '../../app/components/FriendsTab';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('RegisterForm test suite', () => {
    let wrapper = null;
    let expectedProps = {
        getFriendList: jest.fn(),
        acceptFriendRequest: jest.fn(),
        friends: {
            list: {}
        }
    };

    beforeEach(() => {
        wrapper = setup(FriendsTab, expectedProps, undefined, true);
    });

    test('renders without error (empty list of friends)', () => {
        let rootComponent = findByAttr(wrapper, 'nofriends');

        expect(rootComponent.length).toBe(1);
    });

    // test('renders without error (with 1 or more friends)', ()=>{
    //     let props = Object.assign({},expectedProps)
    //     props.friends.list =

    //     let rootComponent = findByAttr(wrapper,"nofriends")

    //     expect(rootComponent.length).toBe(1)

    // })

    test('Check prop types', () => {
        let requiredProps = {
            acceptFriendRequest: jest.fn(),
            getFriendList: jest.fn(),
            friends: {
                list: {}
            }
        };

        let propError = checkProps(FriendsTab, requiredProps);

        // propError is undefined if required props are passed
        expect(propError).toBeUndefined();
    });
});
