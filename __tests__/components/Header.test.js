import React from 'react';
import { shallow } from 'enzyme';
import { TouchableHighlight } from 'react-native';
import Header from '../../app/components/Header';
import { setup, findByAttr } from '../../app/config/testUtil';

test('renders without error without props', () => {
    const wrapper = setup(Header);
    // another shallow wrapper
    let defaultRootComponent = findByAttr(wrapper, 'header-component');
    let propsRootComponent = findByAttr(wrapper, 'header-component2');
    expect(defaultRootComponent.length).toBe(1);
    expect(propsRootComponent.length).toBe(0);
});

test('renders without error on passing props', () => {
    let props = {
        icon: true
    };

    const wrapper = setup(Header, props);
    let defaultRootComponent = findByAttr(wrapper, 'header-component');
    let propsRootComponent = findByAttr(wrapper, 'header-component2');

    expect(defaultRootComponent.length).toBe(0);
    expect(propsRootComponent.length).toBe(1);
});
