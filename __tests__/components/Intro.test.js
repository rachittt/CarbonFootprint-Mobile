import { AsyncStorage } from 'react-native';
import Intro from '../../app/components/Intro';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Intro test suite case', () => {
    let wrapper = null;
    let expectedProps = {};
    AsyncStorage.getItem = jest.fn(() => {
        return new Promise((rej, res) => {
            resolve(JSON.stringify(getTestData()));
        });
    });
    Intro.prototype.checkIsIntroShown = jest.fn(() => {});

    beforeEach(() => {
        wrapper = setup(Intro, expectedProps);
    });

    test('renders without error', () => {
        wrapper.instance().checkIsIntroShown = jest
            .fn()
            .mockImplementation(() => console.log('checkintroshowing'));
        wrapper.instance().forceUpdate();
        let rootComponent = findByAttr(wrapper, 'intro-component');
        expect(rootComponent.length).toBe(1);
    });
});
