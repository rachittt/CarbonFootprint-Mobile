import Terms from '../../app/components/Terms';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Terms test suite case', () => {
    test('renders without error', () => {
        const wrapper = setup(Terms);
        let rootComponent = findByAttr(wrapper, 'terms-component');

        expect(rootComponent.length).toBe(1);
    });
});
