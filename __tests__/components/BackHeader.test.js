import React from 'react';
import BackHeader from '../../app/components/BackHeader';
import { setup, findByAttr } from '../../app/config/testUtil';

test('renders without error', () => {
    const wrapper = setup(BackHeader);
    let rootComponent = findByAttr(wrapper, 'backheader-component');
    let iconComponent = findByAttr(wrapper, 'icon');
    let textComponent = findByAttr(wrapper, 'text');

    expect(rootComponent).toBeTruthy();
    expect(rootComponent.length).toBe(1);
    expect(iconComponent.length).toBe(0);
    expect(textComponent.length).toBe(0);
});

test('passing icon prop and finding icon', () => {
    // const props = {
    //     icon: true
    // }
    // const wrapper = setup(BackHeader,props)
    // let rootComponent = findByAttr(wrapper,"backheader-component")
    // let iconComponent = findByAttr(wrapper, "icon")
    // let textComponent = findByAttr(wrapper, "text")
    // expect(rootComponent).toBeTruthy();
    // expect(rootComponent.length).toBe(1)
    // expect(iconComponent.length).toBe(1)
    // expect(textComponent.length).toBe(0)
});

test('passing text prop and finding icon', () => {
    // const props = {
    //     text: "hello"
    // }
    // const wrapper = setup(BackHeader,props)
    // let rootComponent = findByAttr(wrapper,"backheader-component")
    // let iconComponent = findByAttr(wrapper, "icon")
    // let textComponent = findByAttr(wrapper, "text")
    // expect(rootComponent).toBeTruthy();
    // expect(rootComponent.length).toBe(1)
    // expect(iconComponent.length).toBe(1)
    // expect(textComponent.length).toBe(0)
});
