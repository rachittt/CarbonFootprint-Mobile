import ImageHeader from '../../app/components/ImageHeader';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('FriendsTabBar test suite', () => {
    test('renders without error', () => {
        const wrapper = setup(ImageHeader);
        let rootComponent = findByAttr(wrapper, 'imageheader-component');

        expect(rootComponent.length).toBe(1);
    });
});
