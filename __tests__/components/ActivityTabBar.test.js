import React from 'react';
import { shallow } from 'enzyme';
import { TouchableHighlight } from 'react-native';
import ActivityTabBar from '../../app/components/ActivityTabBar';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

test('renders without error', () => {
    let expectedProps = {
        goToPage: jest.fn(),
        tabsAlt: [
            {
                text: 'Number 1'
            },
            {
                text: 'Number 2'
            }
        ],
        activeTab: 0
    };

    const wrapper = setup(ActivityTabBar, expectedProps);
    let rootComponent = findByAttr(wrapper, 'activitytabbar-component');

    expect(rootComponent.length).toBe(1);
});
