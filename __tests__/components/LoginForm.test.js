import LoginForm from '../../app/components/LoginForm';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('LoginForm test suite', () => {
    let wrapper = null;
    let expectedProps = {
        auth: {}
    };

    beforeEach(() => {
        wrapper = setup(LoginForm, expectedProps);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'loginform-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Check prop types', () => {
        let propError = checkProps(LoginForm, expectedProps);

        // propError is undefined if required props are passed
        expect(propError).toBeUndefined();
    });
});
