import React from 'react';
import FootprintCard from '../../app/components/FootprintCard';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

test('renders without error', () => {
    let expectedProps = {
        onChangeTab: jest.fn(val => val)
    };

    const wrapper = setup(FootprintCard, expectedProps);
    let rootComponent = findByAttr(wrapper, 'footprintcard-component');

    expect(rootComponent).toBeTruthy();
    expect(rootComponent.length).toBe(1);
});

test('simulate clicking on tab', () => {
    let expectedProps = {
        onChangeTab: jest.fn(val => val)
    };

    const wrapper = setup(FootprintCard, expectedProps);
    let rootComponent = findByAttr(wrapper, 'footprintcard-component');
    let firstTabComponent = findByAttr(rootComponent, 'tab').first();

    firstTabComponent.simulate('press');
    // Called once
    expect(expectedProps.onChangeTab.mock.calls).toHaveLength(1);
    // first argument value, called with val = 0
    expect(expectedProps.onChangeTab.mock.calls[0][0]).toBe(0);
    // return value
    expect(expectedProps.onChangeTab.mock.results[0].value).toBe(0);
});

test('Check prop types', () => {
    // Empty object returns error
    let propError = checkProps(FootprintCard, {});
    expect(propError).not.toBeUndefined();

    let expectedProps = {
        onChangeTab: jest.fn()
    };
    propError = checkProps(FootprintCard, expectedProps);

    // propError is undefined if required props are passed
    expect(propError).toBeUndefined();
});
