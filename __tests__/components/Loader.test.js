import Loader from '../../app/components/Loader';
import React from 'react';
// import { setup, findByAttr } from '../../app/config/testUtil';
import { shallow } from 'enzyme';
// imorting setup and findbyattr gives error:
// reducer is expected to be a function
export const setup = (Component, props = {}, initialState = null) => {
    let wrapper = shallow(<Component {...props} />);
    if (initialState) wrapper.setState(state);

    return wrapper;
};

/**
 * Factory function to return the ShallowWrapper with the root as node having testID = val.
 * @param {ShallowWrapper} wrapper - Given ShallowWrapper to search within.
 * @param {string} val - Value of testID attribute for search.
 * @return {ShallowWrapper} - Return ShallowWrapper containing node(s) with the given testID attribute
 */
export const findByAttr = (wrapper, val) => {
    return wrapper.find(`[testID="${val}"]`);
};

describe('Loader test suite', () => {
    test('renders without error', () => {
        let wrapper = setup(Loader);
        let rootComponent = findByAttr(wrapper, 'loader-component');
        let activity = findByAttr(wrapper, 'activity');
        expect(rootComponent.length).toBe(1);
        expect(activity.length).toBe(1);
    });
});
