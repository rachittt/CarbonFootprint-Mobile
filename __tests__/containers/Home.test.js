import Home from '../../app/containers/Home';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Home test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Home, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'home-container');

        expect(rootComponent.length).toBe(1);
    });
});
