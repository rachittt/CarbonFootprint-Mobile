import Settings from '../../app/containers/Settings';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Settings test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Settings, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'settings-container');

        expect(rootComponent.length).toBe(1);
    });
});
