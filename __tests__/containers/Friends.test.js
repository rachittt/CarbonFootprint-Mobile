import Friends from '../../app/containers/Friends';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Friends test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Friends, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'friends-container');

        expect(rootComponent.length).toBe(1);
    });
});
