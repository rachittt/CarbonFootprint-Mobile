import More from '../../app/containers/More';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('More test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(More, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'more-container');

        expect(rootComponent.length).toBe(1);
    });
});
