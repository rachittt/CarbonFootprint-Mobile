import * as firebase from 'firebase';
import * as admin from 'firebase-admin';
import * as FriendsAction from '../../../app/actions/FriendsAction';
import serviceAccount from '../../../app/config/serviceAccountKey';
import { firebaseConfigForTesting } from '../../../app/config/keys';
import { stateMerge, storeFactory } from '../../../app/config/testUtil';

describe('FriendsAction test suite case', () => {
    let store = null;
    const REDUCER_KEY = 'friends';
    const INITIAL_STATE = storeFactory().getState()[REDUCER_KEY];

    beforeEach(() => {
        store = storeFactory();
    });

    test('', () => {});
});
