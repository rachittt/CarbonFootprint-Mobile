import DirectionReducer from '../../app/reducers/direction';
import {
    REQUEST_DIRECTION,
    RECEIVE_DIRECTION,
    SET_SOURCE,
    SET_DESTINATION,
    SET_REGION,
    SET_DISTANCE,
    SET_DURATION,
    NO_DIRECTION
} from '../../app/actions/DirectionAction';

export const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('testing direction reducer suite', () => {
    const EXPECTED_INITIAL_STATE = {
        source: {
            latitude: null,
            longitude: null
        },
        destination: {
            latitude: null,
            longitude: null
        },
        region: {
            latitude: null,
            longitude: null,
            latitudeDelta: null,
            longitudeDelta: null
        },
        coords: null,
        distance: {
            value: null,
            text: null
        },
        duration: {
            value: null,
            text: null
        },
        isFetching: false,
        sourceName: 'Your Location',
        destinationName: 'Where to?'
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = DirectionReducer(undefined, {});

        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('SET_SOURCE type passed', () => {
        let action = {
            type: SET_SOURCE,
            source: 1,
            sourceName: 'one'
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);

        expect(actualState).toEqual(expectedState);
    });

    test('SET_DESTINATION type passed', () => {
        let action = {
            type: SET_DESTINATION,
            destination: 1,
            destinationName: 'one'
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);
        expect(actualState).toEqual(expectedState);
    });

    test('REQUEST_DIRECTION type passed', () => {
        let action = {
            type: REQUEST_DIRECTION
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: true });
        expect(actualState).toEqual(expectedState);
    });

    test('RECEIVE_DIRECTION type passed', () => {
        let action = {
            type: RECEIVE_DIRECTION,
            coords: 'something useful'
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: false });
        expect(actualState).toEqual(expectedState);
    });

    test('SET_REGION type passed', () => {
        let action = {
            type: SET_REGION,
            region: 'something useful'
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);
        expect(actualState).toEqual(expectedState);
    });

    test('SET_DISTANCE type passed', () => {
        let action = {
            type: SET_DISTANCE,
            distance: 10
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);
        expect(actualState).toEqual(expectedState);
    });

    test('SET_DURATION type passed', () => {
        let action = {
            type: SET_DURATION,
            duration: 10
        };
        let actualState = DirectionReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);
        expect(actualState).toEqual(expectedState);
    });

    test('NO_DIRECTION type passed', () => {
        let action = {
            type: NO_DIRECTION
        };
        let actualState = DirectionReducer(undefined, action, {
            isFetching: false,
            coords: null,
            duration: {
                value: null,
                text: null
            },
            distance: {
                value: null,
                text: null
            }
        });

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action);
        expect(actualState).toEqual(expectedState);
    });
});
