import IntroReducer from '../../app/reducers/intro';
import { FIRST_OPEN } from '../../app/actions/IntroAction';

const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('Intro test suite case', () => {
    const EXPECTED_INITIAL_STATE = {
        isFirst: true
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = IntroReducer(undefined, {});

        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('FIRST_OPEN type passed', () => {
        let action = {
            type: FIRST_OPEN
        };
        let actualState = IntroReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFirst: false });

        expect(actualState).toEqual(expectedState);
    });
});
