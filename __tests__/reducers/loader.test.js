import LoaderReducer from '../../app/reducers/loader';
import { LOADER_TOGGLE } from '../../app/actions/LoaderAction';

const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('Intro test suite case', () => {
    const EXPECTED_INITIAL_STATE = {
        isLoading: false
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = LoaderReducer(undefined, {});

        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('LOADER_TOGGLE type passed', () => {
        let action = {
            type: LOADER_TOGGLE
        };
        let actualState = LoaderReducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isLoading: true });

        expect(actualState).toEqual(expectedState);
    });
});
