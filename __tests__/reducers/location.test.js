import Reducer from '../../app/reducers/location';
import { REQUEST_LOCATION, RECEIVE_LOCATION } from '../../app/actions/LocationAction';

const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('Intro test suite case', () => {
    const EXPECTED_INITIAL_STATE = {
        isFetching: false,
        latitude: null,
        longitude: null
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = Reducer(undefined, {});
        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('REQUEST_LOCATION type passed', () => {
        let action = {
            type: REQUEST_LOCATION
        };
        let actualState = Reducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: true });

        expect(actualState).toEqual(expectedState);
    });

    test('RECEIVE_LOCATION type passed', () => {
        let action = {
            type: RECEIVE_LOCATION,
            latitude: 2,
            longitude: 40
        };
        let actualState = Reducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: false });

        expect(actualState).toEqual(expectedState);
    });
});
